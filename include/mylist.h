/*
** mylist.h for mylist.h in /home/el-mou_r/rendu/push_swap/CPE_year_Pushswap/include
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Nov 16 15:56:04 2015 Raidouane EL MOUKHTARI
** Last update Sun Nov 29 00:15:02 2015 Raidouane EL MOUKHTARI
*/

#ifndef MY_LIST_H_
#define MY_LIST_H_

typedef struct	s_list1
{
  char		*name;
  char		*path;
  int		val;
  char		*date;
  int		month;
  int		day;
  int		hour;
  int		min;
  int		sec;
  int		year;
  struct s_list1 *prev;
  struct s_list1 *next;
}		t_list;

typedef struct	s_list
{
  char		*name;
  char		*path;
  int		val;
  char		*date;
  int		month;
  int		day;
  int		hour;
  int		min;
  int		sec;
  int		year;
  struct s_list1 *prev;
  struct s_list1 *next;
}		t_lista;

#endif
