/*
** my.h for my.h in /home/el-mou_r/rendu/push_swap/CPE_year_Pushswap
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Nov  2 15:50:03 2015 Raidouane EL MOUKHTARI
** Last update Sun Nov 29 16:32:33 2015 Raidouane EL MOUKHTARI
*/

#ifndef MY_H_
# define MY_H_
# include "mylist.h"
# include <sys/types.h>
# include <pwd.h>
# include "../CPE_2015_Pushswap/include/my.h"
# include <dirent.h>
# include <stdlib.h>
# include <stdio.h>
# include <grp.h>
# include <time.h>
# include <sys/stat.h>
# include <unistd.h>

typedef struct	s_struct
{
  int		i;
  int		nb_elem;
  int		ac;
  char		**av;
  t_list	*elem;
  t_list	*tmp;
  DIR		*pdir;
  char		*model;
  char		*new;
  char		*s;
  int		*pos;
  int		count;
  int		u;
  int		inc_p;
  int		t;
}		t_struct;

t_list	*create_list_la(int, t_list **la, int *);
void	transform_to_double_list(t_list **la);
void	pa_function(t_struct *, t_list ***,int);
void	pb_function(t_struct *, t_list ***);
void	ra_function(t_struct *, t_list **la);
void	rra_function(t_struct *, t_list **la);
int	function_manage(t_struct *ps, t_list **la, int i);
int	check_already_ifsorted(t_struct *ps, t_list **la, int nb_arg);
void	check_min(t_struct *ps, t_list **la);
int	find_pos_min(t_struct *ps, t_list **la);
void	execute_rotations(t_struct *ps, t_list **la, int inc);
void	filling_list_sorted(t_struct *ps, t_list **la);
char	*my_strcpy(char *dest, char *src);
char	*my_strcat(char *dest, char *src);
char	*all_minuscules(char *s);
void	do_basic_my_ls(t_list **la);
void	display_time(char *str);
void	display_path(char *pathname);
void	display_username(struct stat *file_info);
int	manage_ls_l(struct stat *file_info, char *pathname, int dont_enter);
void	transform_to_double_list_ls(t_list **la);
int	check_options(int ac, char **av);
int	fill_year(t_list **la,int t);
int	fills_ec(t_list **la, int t);
int	fill_min(t_list **la, int t);
int	fill_hour(t_list **la, int t);
int	fill_day(t_list **la);
void	display_my_list_t(t_list *la, int);
t_list	*my_sort_years(t_list **la, int nb_elem);
t_list	*my_sort_months(t_list **la, int nb_elem);
t_list	*my_sort_days(t_list **la, int nb_elem);
t_list	*my_sort_hours(t_list **la, int nb_elem);
t_list	*my_sort_min(t_list **la, int nb_elem);
t_list	*my_sort_sec(t_list **la, int nb_elem);
int	 manage_ls_t(struct stat *file_info , t_list **la, int dont_enter, int nb_elem);
int	manage_option_next(int nb, struct stat *fi, t_list **la, t_struct *ls);
int	*manage_my_parsing(int ac, char **av, t_list **la, t_struct *ls);
int	manage_options(t_struct *l, struct stat *file_info, t_list **la);


#endif
