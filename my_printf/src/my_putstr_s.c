/*
** my_putstr_s.c for my_putstr_S.c in /home/el-mou_r/rendu/PSU_2015_my_printf
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Tue Nov 10 14:07:05 2015 Raidouane EL MOUKHTARI
** Last update Fri Nov 13 21:23:45 2015 Raidouane EL MOUKHTARI
*/

#include "../include/my.h"

void	my_putstr_S_suite(char *recup, t_struct *st)
{
  if (my_strlen(recup) == 0)
    {
      my_putstr("000", st);
      my_putstr(recup, st);
    }
  else if (my_strlen(recup) == 1)
    {
      my_putstr("00", st);
      my_putstr(recup, st);
    }
  else if (my_strlen(recup) == 2)
    {
      my_putstr("0", st);
      my_putstr(recup, st);
    }
  else
    my_putstr(recup, st);
}

void		my_putstr_S(char *str, t_struct *st)
{
  int		i;
  char		*recup;

  i = 0;
  while (str && str[i] != '\0')
    {
      if (str[i] < 32 || str[i] >= 127)
	{
	  my_putchar('\\', st);
	  my_putstr_S_suite(my_getbase_S(str[i], "01234567"),st);
	}
      else
	my_putchar(str[i], st);
      i = i + 1;
    }
}
