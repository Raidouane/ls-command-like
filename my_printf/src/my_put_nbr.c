/*
** my_put_nbr.c for my_put_nbr.c in /home/el-mou_r/rendu/Piscine_C_bistromathique/src
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sat Oct 31 01:30:36 2015 Raidouane EL MOUKHTARI
** Last update Fri Nov 13 21:23:00 2015 Raidouane EL MOUKHTARI
*/

#include "../include/my.h"

void		my_put_nbr(int nb, t_struct *st)
{
  int		mod;

  if (nb < 0)
    {
      my_putchar('-', st);
      nb  = nb * (-1);
    }
  mod = nb % 10;
  if (nb >= 10)
    my_put_nbr(nb / 10, st);
  my_putchar(mod + 48, st);
}
