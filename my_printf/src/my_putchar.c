/*
** my_putchar.c for my_putchar in /home/da-fon_s/rendu/Piscine_C_J06
** 
** Made by samuel da-fonseca
** Login   <da-fon_s@epitech.net>
** 
** Started on  Tue Oct  6 11:11:11 2015 samuel da-fonseca
** Last update Fri Nov 13 21:30:19 2015 Raidouane EL MOUKHTARI
*/

#include "../include/my.h"

void		my_putchar(char c, t_struct *st)
{
  write(1, &c, 1);
  st->nb_char++;
}
