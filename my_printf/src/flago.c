/*
** flago.c for flago.c in /home/el-mou_r/rendu/PSU_2015_my_printf/src
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Tue Nov 10 10:03:13 2015 Raidouane EL MOUKHTARI
** Last update Fri Nov 13 21:23:56 2015 Raidouane EL MOUKHTARI
*/

#include <stdarg.h>
#include "../include/my.h"

void	print_o(va_list va, t_struct *st)
{
  my_getbase(va_arg(va, unsigned int), "01234567", st);
}

void	print_prc(va_list va, t_struct *st)
{
  my_putchar('%', st);
}
