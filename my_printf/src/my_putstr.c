/*
** my_putstr.c for my_putstr in /home/da-fon_s/rendu/Piscine_C_J06
** 
** Made by samuel da-fonseca
** Login   <da-fon_s@epitech.net>
** 
** Started on  Tue Oct  6 11:11:39 2015 samuel da-fonseca
** Last update Fri Nov 13 21:46:57 2015 Raidouane EL MOUKHTARI
*/

#include "../include/my.h"

void		my_putstr(char *str, t_struct *st)
{
  int		i;

  i = 0;
  while (str && str[i] != '\0')
    {
      my_putchar(str[i], st);
      i = i + 1;
    }
}
