/*
** flag_iudcs.c for flag_iudcs.c in /home/el-mou_r/rendu/PSU_2015_my_printf/src
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sun Nov  8 00:34:53 2015 Raidouane EL MOUKHTARI
** Last update Fri Nov 13 21:35:47 2015 Raidouane EL MOUKHTARI
*/

#include <stdarg.h>
#include <stdlib.h>
#include "../include/my.h"

void	print_i(va_list va, t_struct *st)
{
  my_put_nbr(va_arg(va, int), st);
}

void	print_u(va_list va, t_struct *st)
{
  my_getbase(va_arg(va, unsigned int), "0123456789", st);
}

void	print_d(va_list va, t_struct *st)
{
  my_put_nbr(va_arg(va, int), st);
}

void	print_c(va_list va, t_struct *st)
{
  my_putchar(va_arg(va, int), st);
}

void	print_s(va_list va, t_struct *st)
{
  char	*str;

  str = va_arg(va, char *);
  if (str != NULL)
    my_putstr(str, st);
  else if (str == NULL)
    my_putstr("(null)", st);
}
