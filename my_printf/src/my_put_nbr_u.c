/*
** my_put_nbr_u.c for my_put_nbr_u.c in /home/el-mou_r/rendu/PSU_2015_my_printf/src
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sun Nov  8 00:40:53 2015 Raidouane EL MOUKHTARI
** Last update Fri Nov 13 21:35:14 2015 Raidouane EL MOUKHTARI
*/

#include "../include/my.h"

void		my_put_nbr_u(unsigned int nbr, t_struct *st)
{
  if (nbr < 0)
    my_putchar('\0', st);
  else if (nbr / 10 != 0)
    {
      my_put_nbr(nbr / 10, st);
      nbr = nbr % 10;
    }
  my_putchar(nbr + 48, st);
}
