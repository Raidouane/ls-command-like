/*
** flagsbpxx.c for flagsbpxx.c in /home/el-mou_r/rendu/PSU_2015_my_printf/src
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Nov  9 18:41:58 2015 Raidouane EL MOUKHTARI
** Last update Fri Nov 13 21:57:36 2015 Raidouane EL MOUKHTARI
*/

#include <stdarg.h>
#include <stdlib.h>
#include "../include/my.h"

void	print_S(va_list va, t_struct *st)
{
  char	*str;

  str = va_arg(va, char *);
  if (str == NULL)
    my_putstr_S("(null)", st);
  else if (str && str != NULL)
    my_putstr_S(str, st);
}

void	print_b(va_list va,  t_struct *st)
{
  my_getbase(va_arg(va, unsigned int), "01", st);
}

void	print_x(va_list va, t_struct *st)
{
  my_getbase(va_arg(va, unsigned int), "0123456789abcdef", st);
}

void	print_p(va_list va, t_struct *st)
{
  char	*str;

  str = va_arg(va, char *);
  if (str == NULL)
    my_putstr("(nil)", st);
  else
    {
      my_putstr("0x", st);
      my_getbase_pointer((long int)str, "0123456789abcdef", st);
    }
}

void	print_X(va_list va, t_struct *st)
{
  my_getbase(va_arg(va, unsigned int), "0123456789ABCDEF", st);
}
