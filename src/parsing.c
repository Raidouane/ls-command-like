/*
** parsing.c for parsing.c in /home/el-mou_r/rendu/PSU_2015_my_ls_bootstrap
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sun Nov 29 16:21:43 2015 Raidouane EL MOUKHTARI
** Last update Sun Nov 29 17:23:01 2015 Raidouane EL MOUKHTARI
*/

#include <sys/types.h>
#include <pwd.h>
#include <dirent.h>
#include "../include/mylist.h"
#include "../include/my.h"
#include <stdlib.h>
#include <stdio.h>
#include <grp.h>
#include <time.h>
#include <sys/stat.h>
#include <unistd.h>

int		manage_options(t_struct *l, struct stat *file_info, t_list **la)
{
  t_struct	ls;

  if ((ls.pos = malloc(sizeof(int) * 5)) == NULL)
    return (-1);
  if ((ls.model = malloc(5)) == NULL)
    return (-1);
  ls.model = "ltrd\0";
  ls.i = 1;
  if (l->ac == 1)
    ls.i = 0;
  if (l->av[ls.i][0] != '-')
    {
      do_basic_my_ls(la);
      exit(1);
    }
  while (l->av[ls.i][0] != '-' && ls.i < l->ac)
    ls.i++;
  if (ls.i == 2)
    exit (1);
  ls.pos = manage_my_parsing(l->ac, l->av, la, &ls);
  ls.pos[ls.inc_p] = -1;
  ls.tmp = *la;
  manage_option_next(l->nb_elem, file_info, la, &ls);
}

char	*all_minuscules(char *s)
{
  int	i;
  char	*res;

  if ((res = malloc(my_strlen(s) + 1)) == NULL)
    return (NULL);
  i = 0;
  while (s && i < my_strlen(s))
    {
      if ('A' <= s[i] && s[i] <= 'Z')
        res[i] = s[i] + 32;
      else
        res[i] = s[i];
      i++;
    }
  res[i] = '\0';
  return (res);
}

void	display_time(char *str)
{
  int	i;
  int	pass;

  i = 4;
  pass = 0;
  while (str[i] != ' ')
    {

      my_putchar(str[i]);
      i++;
    }
  my_putstr(". ");
  while (str[i] != ':')
    {
      my_putchar(str[i]);
      i++;
    }
  my_putchar(':');
  i++;
  while (str[i] != ':')
    {
      my_putchar(str[i]);
      i++;
    }
}

void	display_path(char *pathname)
{
  my_printf(" %s\n", pathname);
}

void		display_username(struct stat *file_info)
{
  struct passwd	*pwd;
  struct group	*grp;

  pwd = getpwuid(file_info->st_uid);
  my_printf(" %s",pwd->pw_name);
  grp = getgrgid(file_info->st_gid);
  my_printf(" %s", grp->gr_name);
  my_printf(" %d ", file_info->st_size);
}
