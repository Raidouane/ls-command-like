/*
** next_parsing.c for next_parsing.c in /home/el-mou_r/rendu/PSU_2015_my_ls_bootstrap
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sun Nov 29 16:25:45 2015 Raidouane EL MOUKHTARI
** Last update Sun Nov 29 17:12:10 2015 Raidouane EL MOUKHTARI
*/

#include <sys/types.h>
#include <pwd.h>
#include <dirent.h>
#include "../include/mylist.h"
#include "../include/my.h"
#include <stdlib.h>
#include <stdio.h>
#include <grp.h>
#include <time.h>
#include <sys/stat.h>
#include <unistd.h>

void		do_basic_my_ls(t_list **la)
{
  t_list	*tmp;

  tmp = *la;
  while (tmp != (*la)->prev)
    {
      my_printf("%s  ", tmp->name);
      tmp = tmp->next;
    }
  my_printf("%s\n", tmp->name);
}

int	manage_option_next(int nb, struct stat *fi, t_list **la, t_struct *ls)
{
  ls->i = 0;
  ls->t = -1;
  if (ls->pos[ls->i] == -1)
    exit (1);
  while (ls->pos[ls->i] > -1)
    {
      if (ls->pos[ls->i] == 0)
        {
          ls->t++;
          while (ls->tmp != (*la)->prev)
            {
              stat(ls->tmp->path, fi);
              manage_ls_l(fi, ls->tmp->name, ls->t);
              ls->tmp = ls->tmp->next;
            }
          manage_ls_l(fi, ls->tmp->name, ls->t);
        }
      if (ls->pos[ls->i] == 1)
        {
          ls->t++;
          manage_ls_t(fi, la, ls->t, nb);
        }
      ls->i++;
    }
}

int	*manage_my_parsing(int ac, char **av, t_list **la, t_struct *ls)
{
  ls->t = 1;
  ls->u = 0;
  ls->inc_p = 0;
  ls->count = 0;
  while (ls->model[ls->u] != '\0' && av[ls->i][ls->t] != '\0')
    {
      while (av[ls->i][ls->t] != ls->model[ls->u] && ls->model[ls->u] != '\0')
        ls->u++;
      if (av[ls->i][ls->t] == ls->model[ls->u] && av[ls->i][ls->t] != '\0')
        {
          ls->pos[ls->inc_p] = ls->u;
          ls->inc_p++;
          ls->count = ls->count + 1;
	}
      ls->t++;
      ls->u = 0;
    }
  return (ls->pos);
}
