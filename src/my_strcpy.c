/*
** my_strcpy.c for my_strcpy.c in /home/el-mou_r/rendu/PSU_2015_my_ls_bootstrap
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu Nov 26 17:17:50 2015 Raidouane EL MOUKHTARI
** Last update Sat Nov 28 01:45:48 2015 Raidouane EL MOUKHTARI
*/

char	*my_strcpy(char *dest, char *src)
{
  int   ct;

  ct = 0;
  while (src[ct] != '\0')
    {
      dest[ct] = src[ct];
      ct++;
    }
  dest[ct] = '\0';
  return (dest);
}
