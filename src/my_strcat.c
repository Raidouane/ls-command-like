/*
** my_strcat.c for my_strcat.c in /home/el-mou_r/rendu/PSU_2015_my_ls_bootstrap
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu Nov 26 17:16:17 2015 Raidouane EL MOUKHTARI
** Last update Sun Nov 29 14:52:11 2015 Raidouane EL MOUKHTARI
*/

char	*my_strcat(char *dest, char *src)
{
  int	ct;
  int	lg;

  ct = 0;
  lg = my_strlen(dest);
  while (src[ct] != '\0')
    {
      dest[lg] = src[ct];
      lg++;
      ct++;
    }
  dest[lg] = '\0';
  return (dest);
}
