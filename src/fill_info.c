/*
** fill_info.c for fill_info.c in /home/el-mou_r/rendu/PSU_2015_my_ls_bootstrap
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sun Nov 29 01:33:28 2015 Raidouane EL MOUKHTARI
** Last update Sun Nov 29 01:35:49 2015 Raidouane EL MOUKHTARI
*/

#include <sys/types.h>
#include <pwd.h>
#include <dirent.h>
#include "../include/my.h"
#include "../include/mylist.h"
#include <stdlib.h>
#include <stdio.h>
#include <grp.h>
#include <time.h>
#include <sys/stat.h>
#include <unistd.h>

int	fill_year(t_list **la,int t)
{
  char	*str;
  int	i;

  i = 0;
  t++;
  str = malloc(4);
  while ((*la)->date[t] != '\n')
    {
      str[i] = (*la)->date[t];
      i++;
      t++;
    }
  (*la)->year = my_getnbr(str);
  free(str);
}

int	fill_sec(t_list **la, int t)
{
  char	*str;
  int	i;

  i = 0;
  t++;
  str = malloc(3);
  while ((*la)->date[t] != ' ')
    {
      str[i] = (*la)->date[t];
      i++;
      t++;
    }
  str[i] = '\0';
  (*la)->sec = my_getnbr(str);
  free(str);
  return (t);
}

int	fill_min(t_list **la, int t)
{
  char	*s;
  int	i;

  if ((s = malloc(3)) == NULL)
    return (-1);
  i = 0;
  t++;
  while ((*la)->date[t] != ':')
    {
      s[i] = (*la)->date[t];
      i++;
      t++;
    }
  s[i] = '\0';
  (*la)->min = my_getnbr(s);
  free(s);
  return (t);
}

int	fill_hour(t_list **la, int t)
{
  char	*str;
  int	i;

  i = 0;
  if ((str = malloc(3)) == NULL)
    return (-1);
  t++;
  while ((*la)->date[t] != ':')
    {
      str[i] = (*la)->date[t];
      i++;
      t++;
    }
  str[i] = '\0';
  (*la)->hour = my_getnbr(str);
  free(str);
  return (t);
}
