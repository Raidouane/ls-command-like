/*
** ls.c for ls.c in /home/el-mou_r/rendu/PSU_2015_my_ls_bootstrap/src
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Nov 23 10:57:01 2015 Raidouane EL MOUKHTARI
** Last update Sun Nov 29 17:11:44 2015 Raidouane EL MOUKHTARI
*/

#include <sys/types.h>
#include <pwd.h>
#include <dirent.h>
#include "../include/mylist.h"
#include "../include/my.h"
#include <stdlib.h>
#include <stdio.h>
#include <grp.h>
#include <time.h>
#include <sys/stat.h>
#include <unistd.h>

int	manage_ls_l(struct stat *file_info, char *pathname, int dont_enter)
{
  my_printf((S_ISDIR(file_info->st_mode))? "d" : "-");
  my_printf((file_info->st_mode & S_IRUSR)? "r" : "-");
  my_printf((file_info->st_mode & S_IWUSR)? "w" : "-");
  my_printf((file_info->st_mode & S_IXUSR)? "x" : "-");
  my_printf((file_info->st_mode & S_IRGRP)? "r" : "-");
  my_printf((file_info->st_mode & S_IWGRP)? "w" : "-");
  my_printf((file_info->st_mode & S_IXGRP)? "x" : "-");
  my_printf((file_info->st_mode & S_IROTH)? "r" : "-");
  my_printf((file_info->st_mode & S_IWOTH)? "w" : "-");
  my_printf((file_info->st_mode & S_IXOTH)? "x" : "-");
  my_printf(" %d", file_info->st_nlink);
  display_username(file_info);
  display_time(all_minuscules(ctime(&file_info->st_mtime)));
  display_path(pathname);
}

void		transform_to_double_list_ls(t_list **la)
{
  t_list	*tmp_a;
  t_list	*tmp_b;

  tmp_a = *la;
  tmp_b = (*la)->next;
  while (tmp_b->next != NULL)
    {
      tmp_b->prev = tmp_a;
      tmp_b = tmp_b->next;
      tmp_a = tmp_a->next;
    }
  tmp_b->prev = tmp_a;
  tmp_b->next = *la;
  (*la)->prev = tmp_b;
}

int	check_options(int ac, char **av)
{
  int	i;
  int	s;

  i = 1;
  s = 1;
  if (ac == 1)
    return (1);
  while (ac > 2)
    {
      if (av[i][0] == '-' && ((i % 2) == 1))
	s = i + 1;
      i++;
      ac--;
    }
  return (s);
}

t_list	*open_dos(t_struct *ls, struct dirent *di, struct stat fi, t_list **la)
{
  stat(ls->s , &fi);
  while ((di = readdir(ls->pdir)) != NULL)
    {
      if ((ls->new = malloc(my_strlen(ls->s) * 2)) == NULL)
	return (0);
      ls->new[0] = 0;
      ls->new = my_strcpy(ls->new, ls->s);
      if (ls->new[my_strlen(ls->new)] != '/')
	ls->new = my_strcat(ls->new, "/");
      ls->new = my_strcat(ls->new, di->d_name);
      if ((ls->elem = malloc(sizeof(t_list)))== NULL)
	return (0);
      if (di->d_name[0] != '.')
	{
	  stat(ls->new, &fi);
	  ls->elem->name = di->d_name;
	  ls->elem->path = ls->new;
	  ls->elem->date = all_minuscules(ctime(&fi.st_mtime));
	  ls->elem->next = *la;
	  *la = ls->elem;
	  ls->i++;
	}
    }
  return (*la);
}

int		main(int ac, char **av)
{
  t_struct	ls;
  struct dirent *pdirent;
  struct stat   file_info;
  int		rec;
  t_list	*la;

  ls.ac = ac;
  ls.av = av;
  ls.i = 0;
  ls.s = ".";
  la = NULL;
  if (ac != 1)
    {
      rec = check_options(ac, av);
      ls.s = av[rec];
      if (av[rec][0] == '-' && ac < 3)
	ls.s =".";
    }
  rec = check_options(ac, av);
  ls.pdir = opendir(ls.s);
  open_dos(&ls, pdirent, file_info, &la);
  transform_to_double_list_ls(&la);
  ls.nb_elem = ls.i;
  manage_options(&ls, &file_info, &la);
  closedir (ls.pdir);
}
