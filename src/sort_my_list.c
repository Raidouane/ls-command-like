/*
** fill_info.c for fill_info.c in /home/el-mou_r/rendu/PSU_2015_my_ls_bootstrap
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sun Nov 29 01:23:42 2015 Raidouane EL MOUKHTARI
** Last update Sun Nov 29 01:28:58 2015 Raidouane EL MOUKHTARI
*/

#include <sys/types.h>
#include <pwd.h>
#include <dirent.h>
#include "../include/my.h"
#include "../include/mylist.h"
#include <stdlib.h>
#include <stdio.h>
#include <grp.h>
#include <time.h>
#include <sys/stat.h>
#include <unistd.h>

t_list		*my_sort_months(t_list **la, int nb_elem)
{
  t_list	*tmp;
  int		*nb;
  int		i;
  int		u;

  nb = malloc(sizeof(int) * (nb_elem + 1));
  i = nb_elem;
  u = 0;
  tmp = *la;
  while (i > 0)
    {
      nb[u] = tmp->month;
      i--;
      u++;
      tmp = tmp->next;
    }
  nb[u]= -1;
  u = 0;
  tmp = *la;
  *la = create_list_la(nb_elem, &tmp, nb);
  *la = my_sort_years(la, nb_elem);
  free(nb);
  return (*la);
}

t_list		*my_sort_days(t_list **la, int nb_elem)
{
  t_list	*tmp;
  int		*nb;
  int		i;
  int		u;

  nb = malloc(sizeof(int) * (nb_elem + 1));
  i = nb_elem;
  u = 0;
  tmp = *la;
  while (i > 0)
    {
      nb[u] = tmp->day;
      i--;
      u++;
      tmp = tmp->next;
    }
  nb[u]= -1;
  u = 0;
  tmp = *la;
  *la = create_list_la(nb_elem, &tmp, nb);
  fill_months(la);
  *la = my_sort_months(la, nb_elem);
  free(nb);
  return (*la);
}

t_list		*my_sort_hours(t_list **la, int nb_elem)
{
  t_list	*tmp;
  int		*nb;
  int		i;
  int		u;

  nb = malloc(sizeof(int) * (nb_elem + 1));
  i = nb_elem;
  u = 0;
  tmp = *la;
  while (i > 0)
    {
      nb[u] = tmp->hour;
      i--;
      u++;
      tmp = tmp->next;
    }
  nb[u]= -1;
  u = 0;
  tmp = *la;
  *la = create_list_la(nb_elem, &tmp, nb);
  *la = my_sort_days(la, nb_elem);
  free(nb);
  return (*la);
}

t_list		*my_sort_min(t_list **la, int nb_elem)
{
  t_list	*tmp;
  int		*nb;
  int		i;
  int		u;

  nb = malloc(sizeof(int) * (nb_elem + 1));
  i = nb_elem;
  u = 0;
  tmp = (*la)->prev;
  while (i > 0)
    {
      nb[u] = tmp->min;
      i--;
      u++;
      tmp = tmp->next;
    }
  nb[u]= -1;
  tmp = *la;
  u = 0;
  *la = create_list_la(nb_elem, &tmp, nb);
  *la = my_sort_hours(la, nb_elem);
  free(nb);
  return (*la);
}

t_list		*my_sort_sec(t_list **la, int nb_elem)
{
  t_list	*tmp;
  int		*nb;
  int		i;
  int		u;

  nb = malloc(sizeof(int) * (nb_elem + 1));
  i = nb_elem;
  u = 0;
  tmp = (*la)->prev;
  while (i > 0)
    {
      nb[u] = tmp->sec;
      i--;
      u++;
      tmp = tmp->next;
    }
  nb[u]= -1;
  tmp = *la;
  *la = create_list_la(nb_elem, &tmp, nb);
  *la = my_sort_min(la, nb_elem);
  free(nb);
  return (*la);
}
