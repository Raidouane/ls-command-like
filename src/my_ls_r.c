/*
** my_ls_r.c for my_ls_r.c in /home/el-mou_r/rendu/PSU_2015_my_ls_bootstrap
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Tue Nov 24 10:32:28 2015 Raidouane EL MOUKHTARI
** Last update Sun Nov 29 17:38:49 2015 Raidouane EL MOUKHTARI
*/

#include <sys/types.h>
#include <pwd.h>
#include <dirent.h>
#include "../include/my.h"
#include "../include/mylist.h"
#include <stdlib.h>
#include <stdio.h>
#include <grp.h>
#include <time.h>
#include <sys/stat.h>
#include <unistd.h>

void	fill_months(t_list **la)
{
  if ((*la)->date[4] == 'j' && (*la)->date[5] == 'a')
    (*la)->month = 1;
  if ((*la)->date[4] == 'f')
    (*la)->month = 2;
  if ((*la)->date[4] == 'm' && (*la)->date[6] == 'r')
    (*la)->month = 3;
  if ((*la)->date[4] == 'a' && (*la)->date[5] == 'p')
    (*la)->month = 4;
  if ((*la)->date[4] == 'm' && (*la)->date[6] == 'y')
    (*la)->month = 5;
  if ((*la)->date[4] == 'j' && (*la)->date[6] == 'n')
    (*la)->month= 6;
  if ((*la)->date[4] == 'j' && (*la)->date[6] == 'i')
    (*la)->month = 7;
  if ((*la)->date[4] == 'a' && (*la)->date[5] == 'u')
    (*la)->month = 8;
  if ((*la)->date[4] == 's')
    (*la)->month = 9;
  if ((*la)->date[4] == 'o')
    (*la)->month = 10;
  if ((*la)->date[4] == 'n')
    (*la)->month = 11;
  if ((*la)->date[4] == 'd')
    (*la)->month = 12;
}

int	fill_day(t_list **la)
{
  char *str;
  int	i;
  int	t;

  i = 0;
  t = 8;
  str = malloc(3);
  while ((*la)->date[t] != ' ')
    {
      str[i] = (*la)->date[t];
      i++;
      t++;
    }
  str[i] = '\0';
  (*la)->day = my_getnbr(str);
  free(str);
  return (t);
}

void		display_my_list_t(t_list *la, int i)
{
  t_list	*tmp;
  char		**s;
  int		save_i;

  save_i = i;
  tmp = la;
  s = malloc(i * i);
  s[i + 1] = '\0';
  while (i > 0)
    {
      s[i] = tmp->name;
      tmp = tmp->next;
      i--;
    }
  s[i] = tmp->name;
  while (save_i > 1)
    {
      my_putstr(s[i]);
      my_putstr("  ");
      i++;
      save_i--;
    }
  my_putstr(s[i]);
  my_putchar('\n');
  free(s);
}

t_list		*my_sort_years(t_list **la, int nb_elem)
{
  t_list	*tmp;
  int		*nb;
  int		i;
  int		u;

  nb = malloc(sizeof(int) * (nb_elem + 1));
  i = nb_elem;
  u = 0;
  tmp = *la;
  while (i > 0)
    {
      nb[u] = tmp->year;
      i--;
      u++;
      tmp = tmp->next;
    }
  nb[u]= -1;
  u = 0;
  tmp = *la;
  *la = create_list_la(nb_elem, &tmp, nb);
  tmp = *la;
  display_my_list_t(tmp, nb_elem);
  free(nb);
  return (*la);
}

int		manage_ls_t(struct stat *file_info , t_list **la, int enter, int nb)
{
  int		t;
  t_list	*tmp;
  t_list	*first;

  (*la) = (*la)->prev;
  tmp = (*la);
  first = tmp->prev;
  tmp = (*la)->prev;
  while ((*la) != first)
    {
      fill_months(la);
      t = fill_day(la);
      t = fill_hour(la, t);
      t = fill_min(la, t);
      t = fill_sec(la, t);
      fill_year(la, t);
      *la = (*la)->next;
    }
  fill_months(&tmp);
  t = fill_day(la);
  t = fill_hour(la, t);
  t = fill_min(la, t);
  t = fill_sec(la, t);
  fill_year(la, t);
  my_sort_sec(la, nb);
}
