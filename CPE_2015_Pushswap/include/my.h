/*
** my.h for my.h in /home/el-mou_r/rendu/push_swap/CPE_year_Pushswap
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Nov  2 15:50:03 2015 Raidouane EL MOUKHTARI
** Last update Sat Nov 21 10:24:06 2015 Raidouane EL MOUKHTARI
*/

#ifndef MY_H_
# define MY_H_
# include "mylist.h"

typedef struct	s_struct
{
  t_list	*lb;
  int		ct;
  int		nb_argument;
  int		i;
  int		taille_av;
  t_list        *pas_pos;
  int           increment;
  int           pas;
  t_list        *tmp;
  t_list        *tm;
  t_list        *tmpb;
  t_list        *tmpo;
  t_list        *end;
  t_list        *tail;
  t_list        *tmp_inc;
  t_list        *pos_pas;
  int           two;
  int           save_pas;
  int           ii;
  int           save_first_val;
  int           pass;
  int           inc;
  int           cmpt;
  int           nb_arg;
  int           save_inc;
  int		stk_a;
  int		stk_i;
  int		stop_space;
  int           save_i;
}		t_struct;

t_list	*create_list_la(int, char **, t_list *);
void	transform_to_double_list(t_list **la);
void	pa_function(t_struct *, t_list **,int);
void	pb_function(t_struct *, t_list **);
void	ra_function(t_struct *, t_list **la);
void	rra_function(t_struct *, t_list **la);
int	function_manage(t_struct *ps, t_list **la, int i);
int	check_already_ifsorted(t_struct *ps, t_list ***la, int nb_arg);
void	check_min(t_struct *ps, t_list ***la);
int	find_pos_min(t_struct *ps, t_list ***la);
void	execute_rotations(t_struct *ps, t_list ***la, int inc);
void	filling_list_sorted(t_struct *ps, t_list ***la);

#endif
