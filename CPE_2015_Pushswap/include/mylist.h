/*
** mylist.h for mylist.h in /home/el-mou_r/rendu/push_swap/CPE_year_Pushswap/include
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Nov 16 15:56:04 2015 Raidouane EL MOUKHTARI
** Last update Tue Nov 17 15:41:10 2015 Raidouane EL MOUKHTARI
*/

#ifndef MY_LIST_H_
#define MY_LIST_H_

typedef struct	s_list1
{
  int		val;
  struct s_list1 *prev;
  struct s_list1 *next;
}		t_list;

#endif
