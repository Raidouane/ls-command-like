/*
** my_putchar.c for my_putchar in /home/da-fon_s/rendu/Piscine_C_J06
** 
** Made by samuel da-fonseca
** Login   <da-fon_s@epitech.net>
** 
** Started on  Tue Oct  6 11:11:11 2015 samuel da-fonseca
** Last update Wed Oct  7 12:30:15 2015 samuel da-fonseca
*/

void	my_putchar(char c)
{
  write(1, &c, 1);
}
