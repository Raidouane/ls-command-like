/*
** push_swap.c for push_swap.c in /home/el-mou_r/rendu/push_swap/CPE_year_Pushswap
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Nov  2 15:17:28 2015 Raidouane EL MOUKHTARI
** Last update Sat Nov 21 11:17:13 2015 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include <stdio.h>
#include "../include/my.h"
#include "../include/mylist.h"

void	filling_list_sorted(t_struct *ps, t_list ***la)
{
  int	stop_space;

  stop_space = 0;
  while (ps->stk_i > 1)
    {
      if (ps->stk_i == 2)
        stop_space = 1;
      pa_function(ps, (*la), stop_space);
      ps->stk_i--;
    }
}

void			transform_to_double_list(t_list **la)
{
  t_list		*tmp_a;
  t_list		*tmp_b;

  tmp_a = *la;
  tmp_b = (*la)->next;
  while (tmp_b->next != NULL)
  {
    tmp_b->prev = tmp_a;
    tmp_b = tmp_b->next;
    tmp_a = tmp_a->next;
  }
  tmp_b->prev = tmp_a;
  tmp_b->next = *la;
  (*la)->prev = tmp_b;
}

t_list			*create_list_la(int i, char **nb, t_list *la)
{
  t_struct		ps;
  t_list		*elem;
  t_list		*prev;

  ps.i = i - 1;
  la = NULL;
  ps.lb = NULL;
  while (ps.i != 0)
    {
      elem = malloc(sizeof(*elem));
      elem->val = my_getnbr(nb[ps.i]);
      elem->next = la;
      la = elem;
      ps.i = ps.i - 1;
    }
  transform_to_double_list(&la);
  function_manage(&ps, &la, i);
  return (la);
}

int		main(int ac, char **av)
{
  t_list	*la;
  t_list	*tmp;

  if (ac == 2)
    {
      my_putchar('\n');
      return (1);
    }
  if (ac <= 1)
    return (1);
  la = create_list_la(ac, av, la);
  if (la == NULL)
    return (1);
  while (la != la->prev)
    {
      tmp = la->next;
      if (la)
	free(la);
      la = tmp;
    }
  if (la)
    free(la);
  la = NULL;
  return (1);
}
