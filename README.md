The ls command lists the contents of, and optional information about, directories and files. With no options, ls lists the files contained in the current directory, sorting them alphabetically.

`ls [option ...] [file]...`

Made in 2015
