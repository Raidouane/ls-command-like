##
## Makefile for Makefile in /home/el-mou_r/rendu/push_swap/CPE_year_Pushswap
## 
## Made by Raidouane EL MOUKHTARI
## Login   <el-mou_r@epitech.net>
## 
## Started on  Mon Nov  2 16:00:35 2015 Raidouane EL MOUKHTARI
## Last update Sun Nov 29 17:50:55 2015 Raidouane EL MOUKHTARI
##

CC	= gcc -g

RM	= rm

NAME	= my_ls

FLAG	= -Iinclude

SRCPS	=CPE_2015_Pushswap/src/push_swap.c		\
	CPE_2015_Pushswap/src/pushswap.c		\
	CPE_2015_Pushswap/src/push_swap_operation.c	\

SRCMYP	=my_printf/src/my_putchar.c		\
        my_printf/src/my_printf.c		\
        my_printf/src/my_putstr.c		\
        my_printf/src/my_put_nbr.c		\
        my_printf/src/my_putbase.c		\
        my_printf/src/my_putbase_s.c		\
        my_printf/src/my_putbase_pointer.c	\
        my_printf/src/flago.c			\
        my_printf/src/flagsbpxx.c		\
        my_printf/src/my_getnbr.c		\
        my_printf/src/my_put_nbr_u.c		\
        my_printf/src/my_putstr_s.c		\
        my_printf/src/my_revstr.c       	\
        my_printf/src/flag_iudcs.c		\
        my_printf/src/my_strlen.c		\
        my_printf/src/my_check_of_printf.c


SRC	=src/my_ls.c			\
	src/fill_info.c			\
	src/sort_my_list.c		\
	src/parsing.c			\
	src/next_parsing.c			\
	src/my_putstr.c			\
	src/my_strlen.c			\
	src/my_putchar.c		\
	src/my_ls_r.c			\
	src/my_strcpy.c			\
	src/my_strcat.c			\

OBJPS	= $(SRCPS:.c=.o)

OBJMYP	= $(SRCMYP:.c=.o)

OBJ	= $(SRC:.c=.o)

$(NAME):	$(OBJMYP) $(OBJ) $(OBJPS)
	ar rc my_printf.a $(OBJMYP)
	ranlib my_printf.a
	$(CC) $(OBJ) $(OBJPS) my_printf.a $(FLAG) -o $(NAME)

all:	$(NAME)

clean:
	$(RM) -f $(OBJ)

fclean:	clean
	$(RM) -f my_printf.a
	$(RM) -f $(NAME)

re:	fclean all
